data "aws_ami" "gitlab_runner" {
  most_recent = "true"

  filter {
    name   = "name"
    values = ["sk-dev-spinnaker-ami*"]
  }
}

resource "aws_instance" "spinnaker" {
  count = "${var.enable_gitlab_runner ? 1 : 0}"

  ami           = "${data.aws_ami.gitlab_runner.id}"
  instance_type = "${var.gitlab_runner_instance_type}"

  tags        = "${module.ec2_gitlab_runner_label.tags}"
  volume_tags = "${module.ec2_gitlab_runner_label.tags}"

  vpc_security_group_ids = ["${aws_security_group.this.id}"]
  subnet_id              = "${module.vpc.public_subnets[0]}"
  key_name               = "${module.ssh_key_pair.key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  depends_on = [
    "aws_security_group.this",
  ]
}
