stage = "dev"
environment="development"

#-----
# VPC
#-----

cidr = "10.1.0.0/16"
public_subnets = ["10.1.101.0/24"]

## DNS
enable_dns_hostnames = "true"
enable_dns_support = "true"

assign_generated_ipv6_cidr_block = "true"

## One NAT Gateway per subnet
enable_nat_gateway = "true"
single_nat_gateway = "false"
one_nat_gateway_per_az = "false"
